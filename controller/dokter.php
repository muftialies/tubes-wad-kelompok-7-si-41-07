<?php 
	require_once('koneksi.php');
	// $koneksi = mysqli_connect('localhost','root','','puskesmas');

	if (isset($_POST['insert'])) {
		$nama_dokter = $_POST['adminDrNama'];
		$jk_dokter = $_POST['adminDrJK'];
		$spesialis_dokter = $_POST['adminDrSpesialis'];
		$email_dokter = $_POST['adminDrEmail'];
		$password_dokter = $_POST['adminDrPassword'];
		$gambar_dokter = $_FILES["adminDrGambar"]["name"];

		$file_tmp = $_FILES['adminDrGambar']['tmp_name'];

		move_uploaded_file($file_tmp, '../img/dokter/'.$gambar_dokter);	

		$insertdokter = "INSERT INTO `dokter` VALUES ('', '".$nama_dokter."', '".$jk_dokter."', '".$spesialis_dokter."', 'img/dokter/".$gambar_dokter."')";

		$query1 = mysqli_query($koneksi, $insertdokter);

		$getId = "SELECT `id_dokter` FROM `dokter` ORDER BY `id_dokter` DESC LIMIT 1";

		$query2 = mysqli_query($koneksi, $getId);			

		$hasil = mysqli_fetch_assoc($query2);

		$regisaccount = "INSERT INTO `account` VALUES ('','".$email_dokter."','".$password_dokter."','dokter', ".$hasil["id_dokter"].")";

		$query3 = mysqli_query($koneksi, $regisaccount);

		if ($query1 > 0 && $query3 > 0) {
			?>
			<script type="text/javascript">
				alert("Data Dokter Berhasil ditambahkan!");
			</script>
			<?php	
		}
	} else if (isset($_POST['update'])) {
		$id_dokter = $_POST['adminDrIdUpdate'];
		$nama_dokter = $_POST['adminDrNamaUpdate'];
		$jk_dokter = $_POST['adminDrJKUpdate'];
		$spesialis_dokter = $_POST['adminDrSpesialisUpdate'];
		$email_dokter = $_POST['adminDrEmailUpdate'];
		$password_dokter = $_POST['adminDrPasswordUpdate'];

		$updateDokter = "UPDATE `dokter` SET `nama_dokter`='".$nama_dokter."', `jk_dokter`='".$jk_dokter."', `spesialis`='".$spesialis_dokter."' WHERE `id_dokter`='".$id_dokter."'";

		$query = mysqli_query($koneksi, $updateDokter);

		$updateAccount = "UPDATE `account` SET `email_account`='$email_dokter', `password_account`='$password_dokter' WHERE jenis_account = 'dokter' AND `id_jenis_account`='$id_dokter'";

		$query1 = mysqli_query($koneksi, $updateAccount);

		if ($query > 0 && $query1 > 0) {?>
			<script type="text/javascript">
				alert("Data Dokter berhasil diupdate!");	
			</script>
		<?php
		}

	} else if (isset($_GET['delete'])) {
		$id_dokter = $_GET['id_dokter'];

		$deleteDokter = "DELETE FROM `dokter` WHERE `id_dokter`='".$id_dokter."'";

		$query = mysqli_query($koneksi, $deleteDokter);

		$deleteAccount = "DELETE FROM `account` WHERE `jenis_account`='dokter' AND `id_jenis_account`='".$id_dokter."'";

		$query1 = mysqli_query($koneksi, $deleteAccount);

		if ($query > 0 && $query1 > 0) {?>
			<script type="text/javascript">
				alert("Data Dokter berhasil dihapus!");	
			</script>
		<?php
		}

	}
	?>
	<script type="text/javascript">
		window.location.replace("../admindokter.php");
	</script>
	<?php

 ?>