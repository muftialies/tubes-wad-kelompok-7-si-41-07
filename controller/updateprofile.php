<?php  
	session_start();
	require_once('koneksi.php');
	// $koneksi = mysqli_connect('localhost','root','','puskesmas');

	if (isset($_POST['updatePasien'])) {
		$id_pasien = $_SESSION['id_pasien'];
		$nama_pasien = $_POST['upPsNama'];
		$jk_pasien = $_POST['upPsJK'];
		$umur_pasien = $_POST['upPsUmur'];
		$alamat_pasien = $_POST['upPsAlamat'];
		$telepon_pasien = $_POST['upPstelepon'];

		$update = "UPDATE `pasien` SET `nama_pasien`='$nama_pasien', `jk_pasien`='$jk_pasien', `umur_pasien`='$umur_pasien', `alamat_pasien`='$alamat_pasien', `telepon_pasien`='$telepon_pasien' WHERE `id_pasien`='$id_pasien'";

		$query = mysqli_query($koneksi, $update);

		if ($query > 0) {
			$_SESSION['name'] = $nama_pasien;
			?>
			<script type="text/javascript">
				alert("Berhasil Update Profile Pasien");	
			</script>
		<?php
		}else{?>
			<script type="text/javascript">
				alert("Gagal Update Profile Pasien");	
			</script>
		<?php
		}	
		?>
		<script type="text/javascript">
			window.location.replace("../profile.php");
		</script>
		<?php
	} elseif (isset($_POST['updateDokter'])) {
		$id_dokter = $_SESSION['id_dokter'];
		$nama_dokter = $_POST['upDrNama'];
		$jk_dokter= $_POST['upDrJK'];
		$spesialis_dokter = $_POST['upDrSpesialis'];

		$update = "UPDATE `dokter` SET `nama_dokter`='$nama_dokter', `jk_dokter`='$jk_dokter', `spesialis`='$spesialis_dokter' WHERE `id_dokter`='$id_dokter'";

		$query = mysqli_query($koneksi, $update);

		if ($query > 0) {
			$_SESSION['name'] = $nama_dokter;
			?>
			<script type="text/javascript">
				alert("Berhasil Update Profile Dokter");	
			</script>
		<?php
		}else{?>
			<script type="text/javascript">
				alert("Gagal Update Profile Dokter");	
			</script>
		<?php
		}

		?>
		<script type="text/javascript">
			window.location.replace("../profiledokter.php");
		</script>
		<?php
	}
?>