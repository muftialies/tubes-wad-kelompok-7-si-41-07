<?php  
	session_start();
	require_once('koneksi.php');
	// $koneksi = mysqli_connect('localhost','root','','puskesmas');

	if (isset($_POST['resetPassword'])) {
		$id_account = $_SESSION['id_user'];
		$passwordLama = $_POST['passwordLama'];
		$passwordLamaAsli = $_POST['passwordLamaAsli'];
		$passwordBaru = $_POST['passwordBaru'];
		$passwordBaruConv = $_POST['passwordBaruConv'];

		if ($passwordBaru != $passwordBaruConv) {
			?>
			<script type="text/javascript">
				alert("Password baru dan Confirmnya tidak sama!!");	
			</script>
			<?php
		} elseif ($passwordLama != $passwordLamaAsli) {
			?>
			<script type="text/javascript">
				alert("Password lama belum benar");	
			</script>
			<?php
		} else {
			$update = "UPDATE `account` SET `password_account`='$passwordBaru' WHERE `id_account`='$id_account'";

			$query = mysqli_query($koneksi, $update);

			if ($query > 0) {?>
				<script type="text/javascript">
					alert("Berhasil Reset Password");	
				</script>
			<?php
			}else{?>
				<script type="text/javascript">
					alert("Gagal Reset Password");	
				</script>
			<?php
			}	
		}

		if ($_SESSION['status'] == "dokter") {?>
			<script type="text/javascript">
				window.location.replace("../profiledokter.php");
			</script>
		<?php
		} else {?>
			<script type="text/javascript">
				window.location.replace("../profile.php");
			</script>
		<?php
		}
	}	
?>