<?php 
    session_start();
    require_once('controller/koneksi.php');
    if (!(isset($_SESSION["status"]))  && $_SESSION["status"] == "pasien" && !(isset($_GET["konsul"]))) {
        ?>
        <script type="text/javascript">
            window.location.replace("home.php");
        </script>
        <?php
    }
    $id_konsultasi = $_GET["konsul"];
    // $koneksi =mysqli_connect("localhost","root","","puskesmas");
    $pasiens = "SELECT * FROM `pasien` INNER JOIN konsultasi ON konsultasi.id_pasien = pasien.id_pasien
     WHERE id_konsultasi = $id_konsultasi";
    $query = mysqli_query($koneksi, $pasiens);

    $pasien = mysqli_fetch_assoc($query);
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        Form Konsultasi Dokter - Puskesmas Mufti DKK
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">



</head>

<body>

    <!-- *** TOPBAR ***
       _________________________________________________________ -->
    <div id="top" style="background-color: #46B9EA">
        <div class="container">
            <div class="col-md-6 offer" data-animate="fadeInDown">
            </div>
            <div class="col-md-6" data-animate="fadeInDown">
                <ul class="menu">
                    <?php if (isset($_SESSION["status"])): ?>
                        <li>
                            <?php if ((isset($_SESSION["status"])) && $_SESSION['status'] == "dokter"): ?>
                                <a href="profiledokter.php">Hai, <?php echo $_SESSION['name']; ?></a>
                            <?php else: ?>
                                <a href="profile.php">Hai, <?php echo $_SESSION['name']; ?></a>
                            <?php endif ?>
                        </li>
                        <li>
                            <a href="controller/logout.php">Logout</a>
                        </li>
                    <?php else: ?>
                        <li>
                            <a href="login.php">Login</a>
                        </li>
                        <li>
                            <a href="register.php">Register</a>
                        </li>
                    <?php endif ?>
                </ul>
            </div>
        </div>

    </div>

    <!-- *** TOP BAR END *** -->

    <!-- *** NAVBAR ***
       _________________________________________________________ -->

    <div class="navbar navbar-default yamm" role="navigation" id="navbar" >
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand home" href="home.php" data-animate-hover="bounce">
                    <img src="img/logoWeb.png" alt="Obaju logo" class="hidden-xs" style="height: 7rem;">
                </a>
            </div>
            <!--/.navbar-header -->

            <div class="navbar-collapse collapse" id="navigation">

                <ul class="nav navbar-nav navbar-right" style="height: 8rem;">
                    <li><a href="home.php">Home</a>
                    </li>
                    <li><a href="about.php">About</a>
                    </li>
                    <li><a href="layanan.php">Layanan & Fasilitas</a>
                    </li>
                    <li><a href="jadwalDokter.php">Jadwal</a>
                    </li>
                    <li><a href="antriankonsultasi.php">Antrian</a>
                    </li>
                    <?php if ((isset($_SESSION["status"])) && $_SESSION['status'] == "dokter"): ?>
                        <li><a href="listkonsultasidokter.php">Konsultasi</a>
                        </li>
                        <li><a href="listrujukandokter.php">Rujukan</a>
                        </li>
                    <?php else: ?>
                        <li><a href="listkonsultasi.php">Konsultasi</a>
                        </li>
                    <?php endif ?>
                </ul>

            </div>
            <!--/.nav-collapse -->

            <div class="navbar-buttons">

                <div class="navbar-collapse collapse right" id="search-not-mobile">
                    <button type="button" class="btn navbar-btn btn-primary" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                </div>

            </div>

            <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>

            </div>
            <!--/.nav-collapse -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /#navbar -->

    <!-- *** NAVBAR END *** -->

    <div id="content">
            <div class="container">
                <form action="controller/konsultasi.php" method="post" onsubmit="if(!confirm('Apakah Anda Yakin?')) {return false;}">
                <div class="col-md-6">
                    <div class="box">
                        <center>
                            <h2>DATA PASIEN</h2>
                        </center>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jadwaldokter">Nama Pasien</label>
                                    <input type="text" class="form-control" disabled value="<?php echo $pasien['nama_pasien']; ?>">
                                </div>  
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jadwaldokter">Jenis Kelamin Pasien</label>
                                    <input type="text" class="form-control" disabled value="<?php echo $pasien['jk_pasien']; ?>">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jadwaldokter">Umur Pasien</label>
                                    <input type="text" class="form-control" disabled value="<?php echo $pasien['umur_pasien']; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="jadwaldokter">No Telp Pasien</label>
                                    <input type="text" class="form-control" disabled value="<?php echo $pasien['telepon_pasien']; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tulis keluhan kamu</label>
                            <textarea placeholder="isi disini.." name="konsulKeluhan" rows="4" cols="50" class="form-control" disabled><?php echo $pasien['keluhan_konsultasi']; ?></textarea>
                        </div>   
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box">
                        <h1>Hasil Konsultasi</h1>
                        <form action="controller/konsultasi.php" method="post">
                        <div class="form-group">
                            <input type="hidden" name="idKonsul" value="<?php echo $id_konsultasi; ?>">
                            <textarea rows="10" cols="50" class="form-control" name="hasilKonsul"><?php echo $pasien['hasil_konsultasi']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="hasil" value="Hasil Konsultasi">
                        </div>
                        </form>
                    </div>
                </div>
            </form>
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
        
        
        <!-- /#content -->

        <!-- *** FOOTER ***
           _________________________________________________________ -->
           <div id="footer" data-animate="fadeInUp">
            <div class="container">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4" style="float: center;">

                        <h4>Stay in touch</h4>

                        <p class="social">
                            <a href="#" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter external" data-animate-hover="shake"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="gplus external" data-animate-hover="shake"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="email external" data-animate-hover="shake"><i class="fa fa-envelope"></i></a>
                        </p>


                    </div>
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->




        <!-- *** COPYRIGHT ***
           _________________________________________________________ -->
           <div id="copyright"  style="background-color: #46B9EA; color: white;">
            <div class="container">
                <div class="col-md-12">
                    <p align="center">© 2019 Puskesmas MUFTI DKK</p>

                </div>
            </div>
        </div>
        <!-- *** COPYRIGHT END *** -->



    </div>
    <!-- /#all -->


    

    <!-- *** SCRIPTS TO INCLUDE ***
       _________________________________________________________ -->
       <script src="js/jquery-1.11.0.min.js"></script>
       <script src="js/bootstrap.min.js"></script>
       <script src="js/jquery.cookie.js"></script>
       <script src="js/waypoints.min.js"></script>
       <script src="js/modernizr.js"></script>
       <script src="js/bootstrap-hover-dropdown.js"></script>
       <script src="js/owl.carousel.min.js"></script>
       <script src="js/front.js"></script>


   </body>

   </html>