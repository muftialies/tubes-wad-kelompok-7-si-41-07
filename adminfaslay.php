<?php 
    session_start();
    require_once('controller/koneksi.php');
    if (!(isset($_SESSION["status"])) || $_SESSION["status"] != "admin") {
        ?>
        <script type="text/javascript">
            window.location.replace("home.php");
        </script>
        <?php
    }
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        Halaman Admin Fasilitas & Layanan
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">



</head>

<body>

    <!-- *** TOPBAR ***
       _________________________________________________________ -->
       <div id="top" style="background-color: #46B9EA">
        <div class="container">
            <div class="col-md-6 offer" data-animate="fadeInDown">
            </div>
            <div class="col-md-6" data-animate="fadeInDown">
                <ul class="menu">
                    <li><a href="#">Hai, <?php echo $_SESSION['name']; ?></a>
                    </li>
                    <li><a href="controller/logout.php">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- *** TOP BAR END *** -->

    <!-- *** NAVBAR ***
       _________________________________________________________ -->

       <div class="navbar navbar-default yamm" role="navigation" id="navbar" >
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand home" href="#" data-animate-hover="bounce">
                    <img src="img/logoWeb.png" alt="Obaju logo" class="hidden-xs" style="height: 7rem;">
                </a>
            </div>
            <!--/.navbar-header -->

            <div class="navbar-collapse collapse" id="navigation">

                <ul class="nav navbar-nav navbar-right" style="height: 8rem;">
                    <li><a href="admindokter.php">Admin Dokter</a>
                    </li>
                    <li><a href="adminjadwal.php">Admin Jadwal</a>
                    </li>
                    <li><a href="adminpasien.php">Admin Pasien</a>
                    </li>
                    <li class="active"><a href="adminfaslay.php">Admin Layanan & Fasilitas</a>
                    </li>
                </ul>

            </div>
            <!--/.nav-collapse -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /#navbar -->

    <!-- *** NAVBAR END *** -->


        <div class="card-body">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                    <div class="box">
                    <form action="controller/faslay.php" method="POST" enctype="multipart/form-data">
                        <h2> DATA ADMIN FASILITAS & LAYANAN </h2>
                        <?php 
                        if (empty($_POST['updateID'])) {?>
                            <div class="form-group">
                                <label for="nama">Nama Fasilitas & Layanan</label>
                                <input type="text" class="form-control" name="adminFLNama" aria-describedby="emailHelp" placeholder="Nama Fasilitas & Layanan" required>
                            </div>
                            <div class="form-group">
                                <label for="spesialis">Deskripsi Fasilitas & Layanan</label>
                                <textarea rows="4" cols="50" class="form-control" name="adminFLDesc" placeholder="Deskripsi Fasilitas & Layanan" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="gambar">Gambar Fasilitas & Layanan</label>
                                <input type="file" class="form-control-file" name="adminFLGambar" accept="image/*" required>
                            </div>
                        <?php
                        } else {
                            $id_faslay = $_POST['updateID'];
                            //$koneksi = mysqli_connect("localhost","root","","puskesmas");
                            $updatefaslay = "SELECT * FROM `fasilitas_layanan` WHERE `id_faslay` = '$id_faslay'";
                            $query2 = mysqli_query($koneksi, $updatefaslay);
                            $datafaslay = mysqli_fetch_assoc($query2);
                        ?>

                            <input type="hidden" name="adminFLIdUpdate" value="<?php echo $datafaslay['id_faslay'] ?>">
                            <div class="form-group">
                                <label for="nama">Nama Fasilitas & Layanan</label>
                                <input type="text" class="form-control" name="adminFLNamaUpdate" aria-describedby="emailHelp" placeholder="Nama Fasilitas & Layanan" required value="<?php echo $datafaslay['faslay_name'] ?>">
                            </div>
                            <div class="form-group">
                                <label for="spesialis">Deskripsi Fasilitas & Layanan</label>
                                <textarea rows="7" cols="50" class="form-control" name="adminFLDescUpdate" placeholder="Deskripsi Fasilitas & Layanan" required><?php echo $datafaslay['faslay_desc'] ?></textarea>
                            </div>
                        <?php
                        }?>
                        <center>
                            <input type="submit" class="btn btn-primary" name="insert" value="Tambah" <?php if(!empty($_POST['updateID'])) echo 'disabled' ; ?>>
                            <input type="submit" class="btn btn-success" name="update" value="Update" <?php if(empty($_POST['updateID'])) echo 'disabled' ; ?>>
                            <a href="" class="btn btn-dark" <?php if(empty($_POST['updateID'])) echo 'disabled' ; ?>>Reset
                            </a>
                        </center>
                    </form>
                    </div>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>

        <div class="container">
            <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Fasilitas & Layanan</th>
                                    <th width="400">Deskripsi Fasilitas & Layanan</th>
                                    <th>Gambar Fasilitas & Layanan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                // $koneksi =mysqli_connect("localhost","root","","puskesmas");
                                $faslays = "SELECT * FROM `fasilitas_layanan`";
                                $query = mysqli_query($koneksi, $faslays);
                                $i = 1;

                                while ($faslay = mysqli_fetch_assoc($query)) {?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $faslay['faslay_name']; ?></td>
                                        <td><?php echo $faslay['faslay_desc']; ?></td>
                                        <td><img src="<?php echo $faslay['faslay_image']; ?>" width="200"></td>
                                        <td>
                                            <form action="" method="post">
                                                <button type="submit" name="updateID" value="<?php echo $faslay['id_faslay']; ?>" class="btn btn-success">Update</button>
                                                <a onclick="if(!confirm('Yakin hapus data ini?')) {return false;}" href="controller/faslay.php?delete=del&id_faslay=<?php echo $faslay["id_faslay"]; ?>" class="btn btn-danger">Delete</a>
                                            </form>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <div class="modal fade" id="update-modal" tabindex="-1" role="dialog" aria-labelledby="update" aria-hidden="true">
            <div class="modal-dialog modal-sm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Login">Update Dokter</h4>
                    </div>
                    <div class="modal-body">
                        
                        <form action="controller/dokter.php" method="post">
                            

                        </form>
                    </div>
                </div>
            </div>
        </div>

                                <!-- /.table-responsive -->
                           
                            <!-- /.content -->

                           
                            
                    <!-- /.box -->


                
                <!-- /.col-md-9 -->

              
                <!-- /.col-md-3 -->

           
            <!-- /.container -->
        
        
        <!-- /#content -->


        <!-- *** FOOTER ***
           _________________________________________________________ -->
           <div id="footer" data-animate="fadeInUp">
            <div class="container">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4" style="float: center;">

                        <h4>Stay in touch</h4>

                        <p class="social">
                            <a href="#" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter external" data-animate-hover="shake"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="gplus external" data-animate-hover="shake"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="email external" data-animate-hover="shake"><i class="fa fa-envelope"></i></a>
                        </p>


                    </div>
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->




        <!-- *** COPYRIGHT ***
           _________________________________________________________ -->
           <div id="copyright"  style="background-color: #46B9EA; color: white;">
            <div class="container">
                <div class="col-md-12">
                    <p align="center">© 2019 Puskesmas MUFTI DKK</p>

                </div>
            </div>
        </div>
        <!-- *** COPYRIGHT END *** -->



    <!-- /#all -->


    

    <!-- *** SCRIPTS TO INCLUDE ***
       _________________________________________________________ -->
       <script src="js/jquery-1.11.0.min.js"></script>
       <script src="js/bootstrap.min.js"></script>
       <script src="js/jquery.cookie.js"></script>
       <script src="js/waypoints.min.js"></script>
       <script src="js/modernizr.js"></script>
       <script src="js/bootstrap-hover-dropdown.js"></script>
       <script src="js/owl.carousel.min.js"></script>
       <script src="js/front.js"></script>


   </body>

   </html>