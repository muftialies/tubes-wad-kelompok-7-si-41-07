<?php 
    session_start();
    require_once('controller/koneksi.php');
    if (!(isset($_SESSION["status"])) || $_SESSION["status"] != "admin") {
        ?>
        <script type="text/javascript">
            window.location.replace("home.php");
        </script>
        <?php
    }
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        Halaman Admin Dokter
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">



</head>

<body>

    <!-- *** TOPBAR ***
       _________________________________________________________ -->
       <div id="top" style="background-color: #46B9EA">
        <div class="container">
            <div class="col-md-6 offer" data-animate="fadeInDown">
            </div>
            <div class="col-md-6" data-animate="fadeInDown">
                <ul class="menu">
                    <li><a href="#">Hai, <?php echo $_SESSION['name']; ?></a>
                    </li>
                    <li><a href="controller/logout.php">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- *** TOP BAR END *** -->

    <!-- *** NAVBAR ***
       _________________________________________________________ -->

       <div class="navbar navbar-default yamm" role="navigation" id="navbar" >
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand home" href="#" data-animate-hover="bounce">
                    <img src="img/logoWeb.png" alt="Obaju logo" class="hidden-xs" style="height: 7rem;">
                </a>
            </div>
            <!--/.navbar-header -->

            <div class="navbar-collapse collapse" id="navigation">

                <ul class="nav navbar-nav navbar-right" style="height: 8rem;">
                    <li class="active"><a href="admindokter.php">Admin Dokter</a>
                    </li>
                    <li><a href="adminjadwal.php">Admin Jadwal</a>
                    </li>
                    <li><a href="adminpasien.php">Admin Pasien</a>
                    </li>
                    <li><a href="adminfaslay.php">Admin Layanan & Fasilitas</a>
                    </li>
                </ul>

            </div>
            <!--/.nav-collapse -->

            <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>

            </div>
            <!--/.nav-collapse -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /#navbar -->

    <!-- *** NAVBAR END *** -->


        <div class="card-body">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                    <div class="box">
                    <form action="controller/dokter.php" method="POST" enctype="multipart/form-data">
                        <h2> DATA ADMIN DOKTER </h2>
                        <?php 
                        if (empty($_POST['updateID'])) {?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nama">Nama Dokter</label>
                                        <input type="text" class="form-control" name="adminDrNama" aria-describedby="emailHelp" placeholder="Nama Dokter" required>
                                    </div>   
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="jk">JK Dokter</label>
                                        <select name="adminDrJK" class="form-control">
                                            <option value=""></option>
                                            <option value="MALE">Male</option>
                                            <option value="FEMALE">Female</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="spesialis">Spesialis</label>
                                        <input type="text" class="form-control" name="adminDrSpesialis" placeholder="Spesialis" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="gambar">Gambar Dokter</label>
                                        <input type="file" class="form-control-file" name="adminDrGambar" accept="image/*" required>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="spesialis">Email Account Dokter</label>
                                        <input type="email" class="form-control" name="adminDrEmail" placeholder="Email Dokter" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="spesialis">Password Account Dokter</label>
                                        <input type="text" class="form-control" name="adminDrPassword" placeholder="Password Dokter" required>
                                    </div>
                                </div>
                            </div>
                        <?php
                        } else {
                            $id_dktr = $_POST['updateID'];
                            // $koneksi = mysqli_connect("localhost","root","","puskesmas");
                            $updatedokter = "SELECT * FROM `dokter` JOIN account ON dokter.id_dokter = account.id_jenis_account WHERE account.jenis_account = 'dokter' AND dokter.id_dokter='$id_dktr'";
                            $query2 = mysqli_query($koneksi, $updatedokter);
                            $datadokter = mysqli_fetch_assoc($query2);
                        ?>

                            <input type="hidden" name="adminDrIdUpdate" value="<?php echo $datadokter['id_dokter'] ?>">
                            <div class="form-group">
                                <label for="nama">Nama Dokter</label>
                                <input type="text" class="form-control" name="adminDrNamaUpdate" aria-describedby="emailHelp" placeholder="Nama Dokter" required value="<?php echo $datadokter['nama_dokter'] ?>">
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="jk">JK Dokter</label>
                                        <select name="adminDrJKUpdate" class="form-control" value="<?php echo $datadokter['jk_dokter'] ?>">
                                            <option value=""></option>
                                            <option <?php if ($datadokter['jk_dokter'] == "MALE" ) echo 'selected' ; ?> value="MALE">Male</option>
                                            <option <?php if ($datadokter['jk_dokter'] == "FEMALE" ) echo 'selected' ; ?> value="FEMALE">Female</option>
                                        </select>
                                    </div>        
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="spesialis">Spesialis</label>
                                        <input type="text" class="form-control" name="adminDrSpesialisUpdate" placeholder="Spesialis" required value="<?php echo $datadokter['spesialis'] ?>">
                                    </div>        
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="spesialis">Email Account Dokter</label>
                                        <input type="email" class="form-control" name="adminDrEmailUpdate" placeholder="Email Dokter" required value="<?php echo $datadokter['email_account'] ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="spesialis">Password Account Dokter</label>
                                        <input type="text" class="form-control" name="adminDrPasswordUpdate" placeholder="Password Dokter" required value="<?php echo $datadokter['password_account'] ?>"> 
                                    </div>
                                </div>
                            </div>
                        <?php
                        }?>
                        <center>
                            <input type="submit" class="btn btn-primary" name="insert" value="Tambah" <?php if(!empty($_POST['updateID'])) echo 'disabled' ; ?>>
                            <input type="submit" class="btn btn-success" name="update" value="Update" <?php if(empty($_POST['updateID'])) echo 'disabled' ; ?>>
                            <a href="" class="btn btn-dark" <?php if(empty($_POST['updateID'])) echo 'disabled' ; ?>>Reset
                            </a>
                        </center>
                    </form>
                    </div>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>

        <div class="container">
            <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Dokter</th>
                                    <th>JK Dokter</th>
                                    <th>Spesialis</th>
                                    <th>Email Dokter</th>
                                    <th>Password Dokter</th>
                                    <th>Gambar Dokter</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                // $koneksi =mysqli_connect("localhost","root","","puskesmas");
                                $dokters = "SELECT * FROM `dokter` JOIN account ON dokter.id_dokter = account.id_jenis_account WHERE account.jenis_account = 'dokter'";
                                $query = mysqli_query($koneksi, $dokters);
                                $i = 1;

                                while ($dokter = mysqli_fetch_assoc($query)) {?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $dokter['nama_dokter']; ?></td>
                                        <td><?php echo $dokter['jk_dokter']; ?></td>
                                        <td><?php echo $dokter['spesialis']; ?></td>
                                        <td><?php echo $dokter['email_account']; ?></td>
                                        <td><?php echo $dokter['password_account']; ?></td>
                                        <td><img src="<?php echo $dokter['gambar_dokter']; ?>" width="200"></td>
                                        <td>
                                            <form action="" method="post">
                                                <button type="submit" name="updateID" value="<?php echo $dokter['id_dokter']; ?>" class="btn btn-success">Update</button>
                                                <a onclick="if(!confirm('Yakin hapus data ini?')) {return false;}" href="controller/dokter.php?delete=del&id_dokter=<?php echo $dokter["id_dokter"]; ?>" class="btn btn-danger">Delete</a>
                                            </form>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <div class="modal fade" id="update-modal" tabindex="-1" role="dialog" aria-labelledby="update" aria-hidden="true">
            <div class="modal-dialog modal-sm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Login">Update Dokter</h4>
                    </div>
                    <div class="modal-body">
                        
                        <form action="controller/dokter.php" method="post">
                            

                        </form>
                    </div>
                </div>
            </div>
        </div>

                                <!-- /.table-responsive -->
                           
                            <!-- /.content -->

                           
                            
                    <!-- /.box -->


                
                <!-- /.col-md-9 -->

              
                <!-- /.col-md-3 -->

           
            <!-- /.container -->
        
        
        <!-- /#content -->


        <!-- *** FOOTER ***
           _________________________________________________________ -->
           <div id="footer" data-animate="fadeInUp">
            <div class="container">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4" style="float: center;">

                        <h4>Stay in touch</h4>

                        <p class="social">
                            <a href="#" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter external" data-animate-hover="shake"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="gplus external" data-animate-hover="shake"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="email external" data-animate-hover="shake"><i class="fa fa-envelope"></i></a>
                        </p>


                    </div>
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->




        <!-- *** COPYRIGHT ***
           _________________________________________________________ -->
           <div id="copyright"  style="background-color: #46B9EA; color: white;">
            <div class="container">
                <div class="col-md-12">
                    <p align="center">© 2019 Puskesmas MUFTI DKK</p>

                </div>
            </div>
        </div>
        <!-- *** COPYRIGHT END *** -->



    <!-- /#all -->


    

    <!-- *** SCRIPTS TO INCLUDE ***
       _________________________________________________________ -->
       <script src="js/jquery-1.11.0.min.js"></script>
       <script src="js/bootstrap.min.js"></script>
       <script src="js/jquery.cookie.js"></script>
       <script src="js/waypoints.min.js"></script>
       <script src="js/modernizr.js"></script>
       <script src="js/bootstrap-hover-dropdown.js"></script>
       <script src="js/owl.carousel.min.js"></script>
       <script src="js/front.js"></script>


   </body>

   </html>