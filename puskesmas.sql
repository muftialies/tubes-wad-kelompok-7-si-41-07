-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2019 at 01:15 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `puskesmas`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id_account` int(11) NOT NULL,
  `email_account` varchar(100) NOT NULL,
  `password_account` varchar(50) NOT NULL,
  `jenis_account` enum('pasien','admin','dokter') NOT NULL,
  `id_jenis_account` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id_account`, `email_account`, `password_account`, `jenis_account`, `id_jenis_account`) VALUES
(1, 'admin@admin.com', 'admin', 'admin', 1),
(2, 'muftialies@student.telkomuniversity.ac.id', 'qwertyuiop', 'pasien', 1),
(3, 'iyakiwan18@gmail.com', 'qwertyuiop', 'dokter', 2),
(4, 'raisul@amin.com', 'qwertyuiop', 'pasien', 2),
(5, 'ardisa@gmail.com', 'ardisacantik', 'dokter', 1),
(6, 'nada@gmail.com', 'qwertyuiop', 'dokter', 3),
(7, 'raisul@gmail.com', 'qwertyuiop', 'dokter', 6),
(8, 'intan@gmail.com', 'qwertyuiop', 'dokter', 4),
(9, 'altaf@gmail.com', 'qwertyuiop', 'dokter', 5),
(11, 'axl@gmail.com', 'qwert12345', 'pasien', 3),
(12, 'dummy@gmail.com', '1234567', 'dokter', 10),
(13, 'dummy2@gmail.com', '1234567', 'pasien', 4);

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE `dokter` (
  `id_dokter` int(11) NOT NULL,
  `nama_dokter` varchar(50) NOT NULL,
  `jk_dokter` enum('MALE','FEMALE') NOT NULL,
  `spesialis` varchar(30) NOT NULL,
  `gambar_dokter` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokter`
--

INSERT INTO `dokter` (`id_dokter`, `nama_dokter`, `jk_dokter`, `spesialis`, `gambar_dokter`) VALUES
(1, 'dr.Ardisa', 'FEMALE', 'Dokter Umum', 'img/dokter/DOKTERARDISA.jpg'),
(2, 'dr.Mufti Alie Satriawan', 'MALE', 'Dokter Bedah Hati', 'img/dokter/DOKTERMUFTI.jpg'),
(3, 'dr.Nada', 'FEMALE', 'Dokter Bedah Saraf', 'img/dokter/DOKTERNADA.jpg'),
(4, 'dr.Intan', 'FEMALE', 'Dokter Anestesi', 'img/dokter/DOKTERINTAN.jpg'),
(5, 'dr.Altaf', 'MALE', 'Dokter Hewan', 'img/dokter/DOKTERALTAF.jpg'),
(6, 'dr.Raisul', 'MALE', 'Dokter Psikolog', 'img/dokter/DOKTERRAISUL.jpg'),
(10, 'Dummy', 'MALE', 'Mata', 'img/dokter/kisspng-user-logo-information-service-design-5ba34f88e63387.4679724515374293849429.png');

-- --------------------------------------------------------

--
-- Table structure for table `fasilitas_layanan`
--

CREATE TABLE `fasilitas_layanan` (
  `id_faslay` int(11) NOT NULL,
  `faslay_name` varchar(50) NOT NULL,
  `faslay_desc` text NOT NULL,
  `faslay_image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fasilitas_layanan`
--

INSERT INTO `fasilitas_layanan` (`id_faslay`, `faslay_name`, `faslay_desc`, `faslay_image`) VALUES
(1, 'Rawat Inap', 'Pasien membutuhkan pelayanan lebih intensif\r\nSupaya bagus', 'img/faslay/rawat inap.jpg'),
(2, 'Rawat Jalan', 'Pelayanan medis kepada seorang pasien untuk tujuan pengamatan, diagnosis, pengobatan, rehabilitasi, dan pelayanan kesehatan lainnya, tanpa mengharuskan pasien tersebut dirawat inap', 'img/faslay/rawat jalan.jpg'),
(4, 'Konsultasi', 'Bertukar pikiran untuk mendapatkan kesimpulan', 'img/faslay/konsultasi.jpg'),
(7, 'Rujukan', 'Mendapatkan pelayanan dan pengobatan yang lebih baik', 'img/faslay/rujukan.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL,
  `id_dokter` int(11) NOT NULL,
  `jadwal_hari` varchar(10) NOT NULL,
  `jadwal_ruangan` varchar(30) NOT NULL,
  `jadwal_jam_mulai` time NOT NULL,
  `jadwal_jam_akhir` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `id_dokter`, `jadwal_hari`, `jadwal_ruangan`, `jadwal_jam_mulai`, `jadwal_jam_akhir`) VALUES
(1, 1, 'Senin', 'KU3.01.01', '15:00:00', '21:00:00'),
(2, 2, 'Selasa', 'KU3.01.02', '18:00:00', '21:00:00'),
(3, 3, 'Rabu', 'KU3.01.03', '18:00:00', '21:00:00'),
(4, 4, 'Kamis', 'KU3.01.04', '13:00:00', '17:00:00'),
(5, 5, 'Sabtu', 'KU3.01.05', '10:00:00', '16:00:00'),
(6, 6, 'Jumat', 'KU3.01.06', '14:00:00', '18:00:00'),
(7, 2, 'Kamis', 'Lab Sisjar', '15:00:00', '18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `konsultasi`
--

CREATE TABLE `konsultasi` (
  `id_konsultasi` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `keluhan_konsultasi` text NOT NULL,
  `hasil_konsultasi` text DEFAULT NULL,
  `tanggal_konsultasi` date NOT NULL,
  `status_konsultasi` enum('inputdata','berlangsung','berakhir') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konsultasi`
--

INSERT INTO `konsultasi` (`id_konsultasi`, `id_jadwal`, `id_pasien`, `keluhan_konsultasi`, `hasil_konsultasi`, `tanggal_konsultasi`, `status_konsultasi`) VALUES
(1, 7, 1, 'Malesss', 'Sebaiknya kamu belajar dengan giat\r\njangan mels\r\n-semangatt', '2019-11-20', 'inputdata'),
(2, 5, 1, 'Saya Males kak\r\nbucurahan wkejwojoiajoa', NULL, '2019-11-23', 'inputdata'),
(3, 5, 2, 'Sakit panasss', NULL, '2019-11-24', 'inputdata'),
(4, 1, 1, 'Tubes Wad belum kelarrr', 'kurang obat\r\nsemngatt', '2019-11-25', 'inputdata'),
(5, 1, 1, 'Tubes manprosi tolong dong dokter', 'Hmm dasar anak ngeselin', '2019-11-25', 'inputdata');

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `id_pasien` int(11) NOT NULL,
  `nama_pasien` varchar(100) NOT NULL,
  `jk_pasien` enum('Male','Female') NOT NULL,
  `umur_pasien` int(4) NOT NULL,
  `alamat_pasien` text NOT NULL,
  `telepon_pasien` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`id_pasien`, `nama_pasien`, `jk_pasien`, `umur_pasien`, `alamat_pasien`, `telepon_pasien`) VALUES
(1, 'Mufti Alie Satriawan', 'Male', 13, 'Lumajang', '082244073551'),
(2, 'RAZ', 'Male', 20, 'Lapangan Tennis Tel-U', '082256785464'),
(3, 'Muhammad Axl Bayu', 'Male', 21, 'MEnggel Hilir\r\nSukapura', '08245675678'),
(4, 'dummy2', 'Female', 23, 'Sukabirus', '085666666666');

-- --------------------------------------------------------

--
-- Table structure for table `rujukan`
--

CREATE TABLE `rujukan` (
  `id_rujukan` int(11) NOT NULL,
  `asal_rujukan` varchar(50) NOT NULL,
  `tujuan_rujukan` varchar(50) NOT NULL,
  `tanggal_rujukan` date NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `id_dokter` int(11) DEFAULT NULL,
  `alasan_rujukan` text NOT NULL,
  `hasil_rujukan` text DEFAULT NULL,
  `status_rujukan` enum('menunggu','ditolak','diterima') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rujukan`
--

INSERT INTO `rujukan` (`id_rujukan`, `asal_rujukan`, `tujuan_rujukan`, `tanggal_rujukan`, `id_pasien`, `id_dokter`, `alasan_rujukan`, `hasil_rujukan`, `status_rujukan`) VALUES
(1, 'Posyandu Sukapura', 'Puskesmas Mufti DKK', '2019-12-01', 4, 2, 'Karena butuh perawatan intensif, terkait TBC dan dari sisi posyandu kurang memiliki alat nya', 'Perlu perawatan selama 3 hari bosscu', 'diterima'),
(2, 'Posyandu Sukabirus', 'Puskesmas Mufti DKK', '2019-12-02', 2, NULL, 'butuh bantuan segera', NULL, 'menunggu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id_account`);

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id_dokter`);

--
-- Indexes for table `fasilitas_layanan`
--
ALTER TABLE `fasilitas_layanan`
  ADD PRIMARY KEY (`id_faslay`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`),
  ADD KEY `id_dokter` (`id_dokter`);

--
-- Indexes for table `konsultasi`
--
ALTER TABLE `konsultasi`
  ADD PRIMARY KEY (`id_konsultasi`),
  ADD KEY `id_jadwal` (`id_jadwal`),
  ADD KEY `id_pasien` (`id_pasien`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id_pasien`);

--
-- Indexes for table `rujukan`
--
ALTER TABLE `rujukan`
  ADD PRIMARY KEY (`id_rujukan`),
  ADD KEY `id_pasien` (`id_pasien`),
  ADD KEY `id_dokter` (`id_dokter`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id_account` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `dokter`
--
ALTER TABLE `dokter`
  MODIFY `id_dokter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `fasilitas_layanan`
--
ALTER TABLE `fasilitas_layanan`
  MODIFY `id_faslay` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `konsultasi`
--
ALTER TABLE `konsultasi`
  MODIFY `id_konsultasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id_pasien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rujukan`
--
ALTER TABLE `rujukan`
  MODIFY `id_rujukan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD CONSTRAINT `jadwal_ibfk_1` FOREIGN KEY (`id_dokter`) REFERENCES `dokter` (`id_dokter`);

--
-- Constraints for table `konsultasi`
--
ALTER TABLE `konsultasi`
  ADD CONSTRAINT `konsultasi_ibfk_1` FOREIGN KEY (`id_pasien`) REFERENCES `pasien` (`id_pasien`),
  ADD CONSTRAINT `konsultasi_ibfk_2` FOREIGN KEY (`id_jadwal`) REFERENCES `jadwal` (`id_jadwal`);

--
-- Constraints for table `rujukan`
--
ALTER TABLE `rujukan`
  ADD CONSTRAINT `rujukan_ibfk_1` FOREIGN KEY (`id_pasien`) REFERENCES `pasien` (`id_pasien`),
  ADD CONSTRAINT `rujukan_ibfk_2` FOREIGN KEY (`id_dokter`) REFERENCES `dokter` (`id_dokter`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
