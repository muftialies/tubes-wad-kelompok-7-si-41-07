<?php 
    session_start();
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        About - Puskesmas Mufti DKK
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">



</head>

<body>

    <!-- *** TOPBAR ***
       _________________________________________________________ -->
    <div id="top" style="background-color: #46B9EA">
        <div class="container">
            <div class="col-md-6 offer" data-animate="fadeInDown">
            </div>
            <div class="col-md-6" data-animate="fadeInDown">
                <ul class="menu">
                    <?php if (isset($_SESSION["status"])): ?>
                        <li>
                            <?php if ((isset($_SESSION["status"])) && $_SESSION['status'] == "dokter"): ?>
                                <a href="profiledokter.php">Hai, <?php echo $_SESSION['name']; ?></a>
                            <?php else: ?>
                                <a href="profile.php">Hai, <?php echo $_SESSION['name']; ?></a>
                            <?php endif ?>
                        </li>
                        <li>
                            <a href="controller/logout.php">Logout</a>
                        </li>
                    <?php else: ?>
                        <li>
                            <a href="login.php">Login</a>
                        </li>
                        <li>
                            <a href="register.php">Register</a>
                        </li>
                    <?php endif ?>
                </ul>
            </div>
        </div>

    </div>

    <!-- *** TOP BAR END *** -->

    <!-- *** NAVBAR ***
       _________________________________________________________ -->

    <div class="navbar navbar-default yamm" role="navigation" id="navbar" >
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand home" href="home.php" data-animate-hover="bounce">
                    <img src="img/logoWeb.png" alt="Obaju logo" class="hidden-xs" style="height: 7rem;">
                </a>
            </div>
            <!--/.navbar-header -->

            <div class="navbar-collapse collapse" id="navigation">

                <ul class="nav navbar-nav navbar-right" style="height: 8rem;">
                    <li><a href="home.php">Home</a>
                    </li>
                    <li class="active"><a href="about.php">About</a>
                    </li>
                    <li><a href="layanan.php">Layanan & Fasilitas</a>
                    </li>
                    <li><a href="jadwalDokter.php">Jadwal</a>
                    </li>
                    <li><a href="antriankonsultasi.php">Antrian</a>
                    </li>
                    <?php if ((isset($_SESSION["status"])) && $_SESSION['status'] == "dokter"): ?>
                        <li><a href="listkonsultasidokter.php">Konsultasi</a>
                        </li>
                        <li><a href="listrujukandokter.php">Rujukan</a>
                        </li>
                    <?php else: ?>
                        <li><a href="listkonsultasi.php">Konsultasi</a>
                        </li>
                    <?php endif ?>
                </ul>

            </div>
            <!--/.nav-collapse -->

            <div class="navbar-buttons">

                <div class="navbar-collapse collapse right" id="search-not-mobile">
                    <button type="button" class="btn navbar-btn btn-primary" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                </div>

            </div>

            <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>

            </div>
            <!--/.nav-collapse -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /#navbar -->

    <!-- *** NAVBAR END *** -->
  <div id="all">

        <div id="content">

            <div class="container">
                <div class="col-md-12">
                    <div id="main-slider">
                        <div class="item">
                            

                           <center> <img class="img-responsive" src="img/slide3.jpg" alt="" tyle="height: 5000" width="1200"> </center>
                        </div>
                        <div class="item">
                            <center><img class="img-responsive" src="img/slide2.jpg" alt="" style="height: 5200" width="1200"></center>
                        </div>
                        <div class="item">
                           <center> <img class="img-responsive" src="img/slide5.jpg" alt="" style="height: 5200" width="1200"> </center>
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="img/slide6.jpg" alt="" tyle="height: 5000" width="1200">
                        </div>
                    </div>
                 <!--  /#main-slider -->
                </div>

                <div class="col-sm-12" id="blog-listing">

                    <div class="post">
                        <h2><a href="post.html">Sejarah</a></h2>
                        <hr>
                        <p class="intro">Pusat Kesehatan Masyarakat, atau yang disingkat dan lebih dikenal di Indonesia dengan nama Puskesmas, adalah unit pelaksana teknis (UPT) dinas kesehatan kabupaten/kota yang bertanggungjawab menyelenggarakan pembangunan kesehatan di suatu wilayah kerja UPT. Sebagai unit fungsional pelayanan kesehatan terdepan dalam unit pelaksana teknis dinas kesehatan kabupaten/ kota, tugasnya adalah menyelenggarakan sebagian tugas teknis Dinas Kesehatan Pembangunan Kesehatan. Maksudnya adalah sebagai penyelenggara upaya kesehatan seperti melaksanakan upaya penyuluhan, pencegahan dan penanganan kasus-kasus penyakit di wilayah kerjanya, secara terpadu dan terkoordinasi. Sementara pertanggung jawaban secara keseluruhan ada di Dinkes dan sebagian ada di Puskesmas.</p>
                    </div>


                    <div class="post">
                        <h2><a href="post.html">VISI & MISI</a></h2>
                        <hr>
                        <p class="intro">
                            <ul>
                                <li>Menggerakkan pembangunan berwawasan kesehatan.</li>
                                <li>Memelihara dan meningkatkan status kesehatan individu, keluarga dan masyarakat beserta lingkungannya.</li>
                                <li>Mendorong kemandirian masyarakat untuk hidup sehat.</li>
                                <li>Mengupayakan pelayanan kesehatan yang bermutu..</li>
                            </ul>
                        </p>
                    </div>
                    <div class="post">
                        <h2><a href="post.html">Struktur Organisasi</a></h2>
                        <hr>
                        <p class="intro">VISI DAN MISI PUSKESMAS
Menggerakkan pembangunan berwawasan kesehatan.
Memelihara dan meningkatkan status kesehatan individu, keluarga dan masyarakat beserta lingkungannya.
Mendorong kemandirian masyarakat untuk hidup sehat.
Mengupayakan pelayanan kesehatan yang bermutu..</p>
                    </div>
                </div>
        </div>
    </div>
    
        
        
        <!-- /#content -->

        <!-- *** FOOTER ***
           _________________________________________________________ -->
           <div id="footer" data-animate="fadeInUp">
            <div class="container">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4" style="float: center;">

                        <h4>Stay in touch</h4>

                        <p class="social">
                            <a href="#" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter external" data-animate-hover="shake"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="gplus external" data-animate-hover="shake"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="email external" data-animate-hover="shake"><i class="fa fa-envelope"></i></a>
                        </p>


                    </div>
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->
            </div>
            </div>
            <!-- /.container -->
        </div>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->




        <!-- *** COPYRIGHT ***
           _________________________________________________________ -->
           <div id="copyright"  style="background-color: #46B9EA; color: white;">
            <div class="container">
                <div class="col-md-12">
                    <p align="center">© 2019 Puskesmas MUFTI DKK</p>

                </div>
            </div>
        </div>
        <!-- *** COPYRIGHT END *** -->



    </div>
    <!-- /#all -->


    

    <!-- *** SCRIPTS TO INCLUDE ***
       _________________________________________________________ -->
       <script src="js/jquery-1.11.0.min.js"></script>
       <script src="js/bootstrap.min.js"></script>
       <script src="js/jquery.cookie.js"></script>
       <script src="js/waypoints.min.js"></script>
       <script src="js/modernizr.js"></script>
       <script src="js/bootstrap-hover-dropdown.js"></script>
       <script src="js/owl.carousel.min.js"></script>
       <script src="js/front.js"></script>


   </body>

   </html>