<?php 
    session_start();
    require_once('controller/koneksi.php');
    if (!(isset($_SESSION["status"])) || $_SESSION["status"] != "admin") {
        ?>
        <script type="text/javascript">
            window.location.replace("home.php");
        </script>
        <?php
    }
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        Halaman Admin Pasien
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="LogoPuskesmas.png">



</head>

<body>

    <!-- *** TOPBAR ***
       _________________________________________________________ -->
    <div id="top" style="background-color: #46B9EA">
        <div class="container">
            <div class="col-md-6 offer" data-animate="fadeInDown">
            </div>
            <div class="col-md-6" data-animate="fadeInDown">
                <ul class="menu">
                    <li><a href="#">Hai, <?php echo $_SESSION['name']; ?></a>
                    </li>
                    <li><a href="controller/logout.php">Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- *** TOP BAR END *** -->

    <!-- *** NAVBAR ***
       _________________________________________________________ -->

       <div class="navbar navbar-default yamm" role="navigation" id="navbar" >
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand home" href="#" data-animate-hover="bounce">
                    <img src="img/logoWeb.png" alt="Obaju logo" class="hidden-xs" style="height: 7rem;">
                </a>
            </div>
            <!--/.navbar-header -->

            <div class="navbar-collapse collapse" id="navigation">

                <ul class="nav navbar-nav navbar-right" style="height: 8rem;">
                    <li><a href="admindokter.php">Admin Dokter</a>
                    </li>
                    <li><a href="adminjadwal.php">Admin Jadwal</a>
                    </li>
                    <li class="active"><a href="adminpasien.php">Admin Pasien</a>
                    </li>
                    <li><a href="adminfaslay.php">Admin Layanan & Fasilitas</a>
                    </li>
                </ul>

            </div>
            <!--/.nav-collapse -->

            <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>

            </div>
            <!--/.nav-collapse -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /#navbar -->

    <!-- *** NAVBAR END *** -->

    <div class="card-body">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="box">
                <form action="controller/pasien.php" method="POST" enctype="multipart/form-data">
                    <h2> DATA PASIEN PUSKESMAS </h2>
                    <?php 
                    if (empty($_POST['updateID'])) {?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nama">Nama Pasien</label>
                                    <input type="text" class="form-control" name="adminPasienNama" aria-describedby="emailHelp" placeholder="Nama Pasien" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="jk">Jenis Kelamin Pasien</label>
                                    <select name="adminPasienJK" class="form-control">
                                        <option value=""></option>
                                        <option value="MALE">Male</option>
                                        <option value="FEMALE">Female</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="Umur">Umur Pasien</label>
                                    <input type="number" class="form-control" name="adminPasienUmur" placeholder="Umur" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="TelpPasien">Telp Pasien</label>
                                    <input type="number" class="form-control" name="adminPasienTelp" placeholder="No Telepon Pasien" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="AlamatPasien">Alamat Pasien</label>
                            <textarea class="form-control" name="adminPasienAlamat" placeholder="Alamat" required></textarea>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nama">Email Pasien</label>
                                    <input type="text" class="form-control" name="adminPasienEmail" aria-describedby="emailHelp" placeholder="Email Pasien" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nama">Password Pasien</label>
                                    <input type="text" class="form-control" name="adminPasienPassword" aria-describedby="emailHelp" placeholder="Password Pasien" required>
                                </div>
                            </div>
                        </div>
                        
                         <?php
                        } else {
                            $id_psn = $_POST['updateID'];
                            // $koneksi = mysqli_connect("localhost","root","","puskesmas");
                            $updatepasien = "SELECT * FROM `pasien` JOIN account ON pasien.id_pasien = account.id_jenis_account WHERE account.jenis_account = 'pasien' AND  pasien.id_pasien ='$id_psn'";
                            $query2 = mysqli_query($koneksi, $updatepasien);
                            $datapasien = mysqli_fetch_assoc($query2);
                            ?>
                            <input type="hidden" name="adminPasienIdUpdate" value="<?php echo $id_psn ?>">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nama">Nama Pasien</label>
                                        <input type="text" class="form-control" name="adminPasienNamaUpdate" aria-describedby="emailHelp" placeholder="Nama Pasien" required value="<?php echo $datapasien['nama_pasien'] ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="jk">JK Pasien</label>
                                        <select name="adminPasienJKUpdate" class="form-control" value="<?php echo $datapasien['jk_pasien'] ?>">
                                            <option value=""></option>
                                            <option <?php if ($datapasien['jk_pasien'] == "Male" ) echo 'selected' ; ?> value="Male">Male</option>
                                            <option <?php if ($datapasien['jk_pasien'] == "Female" ) echo 'selected' ; ?> value="Female">Female</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="Umur">Umur Pasien</label>
                                        <input type="number" class="form-control" name="adminPasienUmurUpdate" placeholder="Umur Pasien" required value="<?php echo $datapasien['umur_pasien'] ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="TelpPasien">Telp Pasien</label>
                                        <input type="number" class="form-control" name="adminPasienTelpUpdate" placeholder="Telp Pasien" required value="<?php echo $datapasien['telepon_pasien'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="AlamatPasien">Alamat Pasien</label>
                                <textarea class="form-control" name="adminPasienAlamatUpdate" placeholder="Alamat Pasien" required><?php echo $datapasien['alamat_pasien'] ?></textarea>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nama">Email Pasien</label>
                                        <input type="text" class="form-control" name="adminPasienEmailUpdate" aria-describedby="emailHelp" placeholder="Email Pasien" required value="<?php echo $datapasien['email_account'] ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nama">Password Pasien</label>
                                        <input type="text" class="form-control" name="adminPasienPasswordUpdate" aria-describedby="emailHelp" placeholder="Password Pasien" required value="<?php echo $datapasien['password_account'] ?>">
                                    </div>
                                </div>
                            </div> 
                        <?php
                        }?>
                        <center>
                            <input type="submit" class="btn btn-primary" name="insert" value="Tambah" <?php if(!empty($_POST['updateID'])) echo 'disabled' ; ?>>
                            <input type="submit" class="btn btn-success" name="update" value="Update" <?php if(empty($_POST['updateID'])) echo 'disabled' ; ?>>
                            <a href="" class="btn btn-dark" <?php if(empty($_POST['updateID'])) echo 'disabled' ; ?>>Reset
                            </a>
                        </center>
                </form>
                </div>
            </div>
            <div class="col-sm-3"></div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pasien</th>
                                <th>Jenis Kelamin Pasien</th>
                                <th>Umur Pasien</th>
                                <th>Alamat Pasien</th>
                                <th>Telp Pasien</th>
                                <th>Email Pasien</th>
                                <th>Password Pasien</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            // $koneksi =mysqli_connect("localhost","root","","puskesmas");
                            $pasiens = "SELECT * FROM `pasien` JOIN account ON pasien.id_pasien = account.id_jenis_account WHERE account.jenis_account = 'pasien'";
                            $query = mysqli_query($koneksi, $pasiens);
                            $i = 1;

                            while ($pasien = mysqli_fetch_assoc($query)) {?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $pasien['nama_pasien']; ?></td>
                                    <td><?php echo $pasien['jk_pasien']; ?></td>
                                    <td><?php echo $pasien['umur_pasien']; ?></td>
                                    <td><?php echo $pasien['alamat_pasien']; ?></td>
                                    <td><?php echo $pasien['telepon_pasien']; ?></td>
                                    <td><?php echo $pasien['email_account']; ?></td>
                                    <td><?php echo $pasien['password_account']; ?></td>
                                    <td>
                                    <form action="" method="post">
                                        <button type="submit" name="updateID" value="<?php echo $pasien['id_pasien']; ?>" class="btn btn-success">Update</button>
                                        <a onclick="if(!confirm('Yakin hapus data ini?')) {return false;}" href="controller/pasien.php?delete=delete&id_pasien=<?php echo $pasien["id_pasien"]; ?>" class="btn btn-danger">Delete</a>
                                    </form>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>

                                <!-- /.table-responsive -->
                           
                            <!-- /.content -->

                           
                            
                    <!-- /.box -->


                
                <!-- /.col-md-9 -->

              
                <!-- /.col-md-3 -->

           
            <!-- /.container -->
        
        
        <!-- /#content -->


        <!-- *** FOOTER ***
           _________________________________________________________ -->
           <div id="footer" data-animate="fadeInUp">
            <div class="container">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4" style="float: center;">

                        <h4>Stay in touch</h4>

                        <p class="social">
                            <a href="#" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter external" data-animate-hover="shake"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="gplus external" data-animate-hover="shake"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="email external" data-animate-hover="shake"><i class="fa fa-envelope"></i></a>
                        </p>


                    </div>
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->




        <!-- *** COPYRIGHT ***
           _________________________________________________________ -->
           <div id="copyright"  style="background-color: #46B9EA; color: white;">
            <div class="container">
                <div class="col-md-12">
                    <p align="center">© 2019 Puskesmas MUFTI DKK</p>

                </div>
            </div>
        </div>
        <!-- *** COPYRIGHT END *** -->



    <!-- /#all -->


    

    <!-- *** SCRIPTS TO INCLUDE ***
       _________________________________________________________ -->
       <script src="js/jquery-1.11.0.min.js"></script>
       <script src="js/bootstrap.min.js"></script>
       <script src="js/jquery.cookie.js"></script>
       <script src="js/waypoints.min.js"></script>
       <script src="js/modernizr.js"></script>
       <script src="js/bootstrap-hover-dropdown.js"></script>
       <script src="js/owl.carousel.min.js"></script>
       <script src="js/front.js"></script>


   </body>

   </html>